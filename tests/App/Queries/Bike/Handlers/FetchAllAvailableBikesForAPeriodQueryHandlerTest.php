<?php

namespace App\Queries\Bike\Handlers;

use App\Domain\Bike\Bike;
use App\Domain\Rent\RentPeriod;
use App\Domain\Rent\RentPeriodTest;
use App\Queries\Bike\FetchAllAvailableBikesForAPeriodQuery;
use App\Repositories\Contracts\BikeRepositoryInterface;
use App\Services\Bike\Contracts\BikeAvailabilityServiceInterface;
use Carbon\CarbonImmutable;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class FetchAllAvailableBikesForAPeriodQueryHandlerTest extends TestCase
{
    private $repository;
    private $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(BikeRepositoryInterface::class);
        $this->service = $this->createMock(BikeAvailabilityServiceInterface::class);
    }


    public function testHandle()
    {
        $rentPeriod = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));
        $bike1 = Bike::fromArray(['manufacturer' => 'Yamaha', 'category' => 'MotoGp', 'stock' => '5']);
        $bike2 = Bike::fromArray(['manufacturer' => 'Ktm', 'category' => 'Superbike', 'stock' => '5']);
        $bike3 = Bike::fromArray(['manufacturer' => 'Honda', 'category' => 'MotoGp', 'stock' => '5']);
        $bikes = Collection::make([
            $bike1,
            $bike2,
            $bike3
        ]);
        $this->repository->expects(self::once())->method('fetchAll')->willReturn($bikes);

        $bike1WithCurrentAvailability = clone $bike1;
        $bike1WithCurrentAvailability->stock = 5;
        $bike2WithCurrentAvailability = clone $bike2;
        $bike2WithCurrentAvailability->stock = 1;
        $bike3WithCurrentAvailability = clone $bike3;
        $bike3WithCurrentAvailability->stock = 0;
        $this->service->expects(self::exactly(3))
            ->method('getBikeWithAvailabilityForAPeriod')->withConsecutive(
                [$bike1->id, $rentPeriod],
                [$bike2->id, $rentPeriod],
                [$bike3->id, $rentPeriod])
            ->willReturnOnConsecutiveCalls(
                $bike1WithCurrentAvailability,
                $bike2WithCurrentAvailability,
                $bike3WithCurrentAvailability);

        $handler = new FetchAllAvailableBikesForAPeriodQueryHandler($this->repository, $this->service);
        $result = $handler->handle(new FetchAllAvailableBikesForAPeriodQuery($rentPeriod));

        self::assertCount(2, $result);
        self::assertEquals(Collection::make([$bike1WithCurrentAvailability, $bike2WithCurrentAvailability]), $result);

    }

    public function testListen()
    {
        $handler = new FetchAllAvailableBikesForAPeriodQueryHandler($this->repository, $this->service);
        self::assertEquals(FetchAllAvailableBikesForAPeriodQuery::class, $handler->listen());
    }
}
