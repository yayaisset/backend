<?php
namespace App\Queries\Bike\Handlers;

use App\Domain\Bike\Bike;
use App\Models\BikeModel;
use App\Queries\Bike\FetchAllBikesQuery;
use App\Repositories\Contracts\BikeRepositoryInterface;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class FetchAllBikesQueryHandlerTest extends TestCase
{
    private BikeRepositoryInterface $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(BikeRepositoryInterface::class);
    }

    public function testHandle()
    {
        $bike1 = Bike::fromArray(['manufacturer'=> 'Yamaha', 'category'=>'MotoGp', 'stock'=>'5']);
        $bike2 = Bike::fromArray(['manufacturer'=> 'Ktm', 'category'=>'Superbike', 'stock'=>'5']);
        $result = Collection::make([
            $bike1,
            $bike2
        ]);
        $this->repository->expects(self::once())->method('fetchAll')->willReturn($result);

        $handler = new FetchAllBikesQueryHandler($this->repository);

        $response = $handler->handle(new FetchAllBikesQuery());
        self::assertEquals($result, $response);
    }
    public function testListen()
    {
        $handler = new FetchAllBikesQueryHandler($this->repository);
        self::assertEquals(FetchAllBikesQuery::class, $handler->listen());
    }
}
