<?php

namespace App\Queries\Rent\Handlers;

use App\Models\RentModel;
use App\Queries\Rent\FetchAllRentsQuery;
use App\Repositories\Contracts\RentRepositoryInterface;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class FetchAllRentsQueryHandlerTest extends TestCase
{
    private RentRepositoryInterface $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(RentRepositoryInterface::class);
    }

    public function testHandle()
    {
        $rent1 = new RentModel(['id' => '1']);
        $rent2 = new RentModel(['id' => '2']);
        $result = Collection::make([
            $rent1,
            $rent2
        ]);
        $this->repository->expects(self::once())->method('fetchAll')->willReturn($result);


        $handler = new FetchAllRentsQueryHandler($this->repository);

        $response = $handler->handle(new FetchAllRentsQuery());
        self::assertEquals($result, $response);
    }
    public function testListen()
    {
        $handler = new FetchAllRentsQueryHandler($this->repository);
        self::assertEquals(FetchAllRentsQuery::class, $handler->listen());
    }
}
