<?php
namespace App\Repositories;

use App\CreatesApplication;
use App\Domain\Bike\Bike;
use App\Exceptions\BusinessException;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class BikeRepositoryTest extends TestCase
{
    use CreatesApplication;

    private Application $application;
    private $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->application = $this->createApplication();
        $this->factory = $this->createMock(Factory::class);
        DB::table('bikes')->truncate();
    }

    public function testAddBike()
    {
        $repository = new BikeRepository($this->application, $this->factory);
        $category = 'MotoGp';
        $manufacturer = 'Suzuki';
        $stock = 5;
        $repository->createBike(Bike::fromArray(['category' => $category, 'manufacturer' => $manufacturer, 'stock' => $stock]));

        $list = $repository->fetchAll();

        self::assertCount(1, $list);
        /** @var Bike $bike */
        $bike = $list->get(0);
        self::assertEquals($category, $bike->category->getValue());
        self::assertEquals($manufacturer, $bike->manufacturer->getValue());
        self::assertEquals($stock, $bike->stock);
    }

    public function testFetchAll()
    {
        $repository = new BikeRepository($this->application, $this->factory);

        for ($i=0; $i<10; $i++){
            $category = 'Superbike';
            $manufacturer = 'Ducati';
            $stock = 5;
            $repository->createBike(Bike::fromArray(['category' => $category, 'manufacturer' => $manufacturer, 'stock' => $stock]));
        }
        self::assertCount(10, $repository->fetchAll());
    }

    public function testFindBikeById()
    {
        $repository = new BikeRepository($this->application, $this->factory);
        $category = 'MotoGp';
        $manufacturer = 'Ktm';
        $stock = 5;
        $bike = Bike::fromArray(['category' => $category, 'manufacturer' => $manufacturer, 'stock' => $stock]);
        $repository->createBike($bike);

        $actualBike = $repository->findBikeById($bike->id);

        self::assertEquals($bike, $actualBike);
    }

    public function testUpdateBikeStock()
    {
        $repository = new BikeRepository($this->application, $this->factory);
        $category = 'MotoGp';
        $manufacturer = 'Suzuki';
        $stock = 5;
        $bike = Bike::fromArray(['category' => $category, 'manufacturer' => $manufacturer, 'stock' => $stock]);
        $repository->createBike($bike);

        $actualBike = $repository->findBikeById($bike->id);
        self::assertEquals($stock, $bike->stock);
        $newStock = 6;
        $repository->updateBike($actualBike->id, ['stock' => $newStock]);

        $updatedBike = $repository->findBikeById($bike->id);

        self::assertEquals($newStock, $updatedBike->stock);
    }

    public function testThrowExceptionInUpdateOnUnknownBike()
    {
        $this->expectException(BusinessException::class);
        $this->expectExceptionMessage('Sorry, this bike does not exist');
        $repository = new BikeRepository($this->application, $this->factory);
        $repository->updateBike(Uuid::uuid4(), ['stock' => 4]);
    }
}
