<?php

namespace App\Repositories;

use App\Models\InfrastructureExceptionModel;
use Carbon\CarbonImmutable;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\TestCase;
use App\CreatesApplication;

class InfrastructureExceptionRepositoryTest extends TestCase
{
    use CreatesApplication;

    private Application $application;
    private $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->application = $this->createApplication();
        $this->factory = $this->createMock(Factory::class);
        DB::table('infrastructure_exceptions')->truncate();
    }

    public function testStoreException()
    {
        $repository = new InfrastructureExceptionRepository($this->application, $this->factory);
        $model = InfrastructureExceptionModel::fromArray([
            'title' => 'AnException',
            'message' => 'an exception message',
            'useCase' => 'aUseCase',
            'throwedAt' => new CarbonImmutable(),
        ]);
        $repository->storeException($model);

        $list = $repository->all();

        self::assertCount(1, $list);
        /** @var InfrastructureExceptionModel $exception */
        $exception = $list->get(0);
        self::assertEquals($model->getId(), $exception->getId());
        self::assertEquals($model->getTitle(), $exception->getTitle());
        self::assertEquals($model->getMessage(), $exception->getMessage());
        self::assertEquals($model->getUseCase(), $exception->getUseCase());
        self::assertEquals((new CarbonImmutable())->format('d/m/Y h:i:s'), $exception->getThrowedAt()->format('d/m/Y h:i:s'));
    }
}
