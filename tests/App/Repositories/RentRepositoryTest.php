<?php

namespace App\Repositories;

use App\CreatesApplication;
use App\Domain\Rent\Rent;
use App\Domain\Rent\RentPeriod;
use App\Exceptions\BusinessException;
use Carbon\CarbonImmutable;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class RentRepositoryTest extends TestCase
{
    use CreatesApplication;

    private Application $application;
    private $factory;
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->application = $this->createApplication();
        $this->factory = $this->createMock(Factory::class);
        $this->repository = new RentRepository($this->application, $this->factory);

        DB::table('rents')->truncate();
    }

    public function testCreateRent()
    {
        $name = 'yannick';
        $email = 'yannick@email.fr';
        $bike = Uuid::uuid4();
        $startDate = new CarbonImmutable('2020-01-01');
        $endDate = new CarbonImmutable('2020-01-31');
        $this->repository->createRent(Rent::fromArray([
            'name' => $name,
            'email' => $email,
            'bike' => $bike,
            'startDate' => $startDate,
            'endDate' => $endDate
        ]));

        $list = $this->repository->fetchAll();

        self::assertCount(1, $list);
        /** @var Rent $rent */
        $rent = $list->get(0);
        self::assertEquals($name, $rent->name);
        self::assertEquals($email, $rent->email);
        self::assertEquals($bike, $rent->bike);
        self::assertEquals($startDate, $rent->rentPeriod->getStartDate());
        self::assertEquals($endDate, $rent->rentPeriod->getEndDate());
    }

    public function testFetchAll()
    {
        for ($i = 0; $i < 10; $i++) {
            $name = 'yannick';
            $email = 'yannick@email.fr';
            $bike = Uuid::uuid4();
            $startDate = new CarbonImmutable('2020-01-01');
            $endDate = new CarbonImmutable('2020-01-31');
            $this->repository->createRent(Rent::fromArray([
                'name' => $name,
                'email' => $email,
                'bike' => $bike,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]));
        }
        self::assertCount(10, $this->repository->fetchAll());
    }

    /**
     * @param string $searchStartDate
     * @param string $searchEndDate
     * @param int $rentNumber
     * @testWith        ["2020-01-03", "2020-01-25", 10]
     *                  ["2020-01-30", "2020-02-30",  10]
     *                  ["2019-12-30", "2021-02-30",  10]
     *                  ["2020-02-01", "2020-02-30",  0]
     *                  ["2019-12-15", "2019-12-31",  0]
     */
    public function testCountRentsByPeriod(string $searchStartDate, string $searchEndDate, int $rentNumber)
    {
        $bikeToSearch = Uuid::uuid4();
        $startDate = new CarbonImmutable('2020-01-01');
        $endDate = new CarbonImmutable('2020-01-31');
        for ($i = 0; $i < 10; $i++) {
            $name = 'yannick';
            $email = 'yannick@email.fr';
            $this->repository->createRent(Rent::fromArray([
                'name' => $name,
                'email' => $email,
                'bike' => $bikeToSearch,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]));
        }
        for ($i = 0; $i < 10; $i++) {
            $name = 'yannick';
            $email = 'yannick@email.fr';
            $bike = Uuid::uuid4();
            $this->repository->createRent(Rent::fromArray([
                'name' => $name,
                'email' => $email,
                'bike' => $bike,
                'startDate' => $startDate,
                'endDate' => $endDate
            ]));
        }
        self::assertEquals($rentNumber, $this->repository->countRentsByBikeAndPeriod($bikeToSearch,
            RentPeriod::fromDates(new CarbonImmutable($searchStartDate), new CarbonImmutable($searchEndDate))));
    }

    public function testDeleteRent()
    {
        $rent = Rent::fromArray([
            'name' => 'yannick',
            'email' => 'yannick@email.fr',
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('2020-01-01'),
            'endDate' => new CarbonImmutable('2020-01-31')
        ]);
        $this->repository->createRent($rent);

        $list = $this->repository->fetchAll();
        self::assertCount(1, $list);

        $this->repository->deleteRent($rent->id);

        $list = $this->repository->fetchAll();
        self::assertCount(0, $list);

    }

    public function testThrowBusinessExceptionOnUnknownRent()
    {
        $this->expectException(BusinessException::class);
        $this->expectExceptionMessage('Sorry, this rent does not exist...');
        $this->repository->deleteRent(Uuid::uuid4());
    }
}
