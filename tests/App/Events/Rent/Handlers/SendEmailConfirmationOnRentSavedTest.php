<?php

namespace App\Events\Rent\Handlers;

use App\Domain\Rent\Rent;
use App\Events\Rent\RentSaved;
use App\Exceptions\ConfirmationMailNotSentException;
use App\TestCase;
use Carbon\CarbonImmutable;
use Ramsey\Uuid\Uuid;
use Swift_TransportException;

class SendEmailConfirmationOnRentSavedTest extends TestCase
{
    private $swiftMailer;

    protected function setUp(): void
    {
        parent::setUp();
        $mailer = $this->app->make('mailer');
        $this->swiftMailer = \Mockery::mock($mailer->getSwiftMailer());
        $mailer->setSwiftMailer($this->swiftMailer);
    }

    public function testSendConfirmation()
    {
        $handler = new SendEmailConfirmationOnRentSaved();
        $email = 'yannick@email.fr';
        $rent = Rent::fromArray([
            'name' => 'yannick',
            'email' => $email,
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('2020-01-01'),
            'endDate' => new CarbonImmutable('2020-01-31')
        ]);

        $this->swiftMailer->shouldReceive('send')->once()->withArgs(function (\Swift_Message $mail) use ($email){
            return array_key_exists($email, $mail->getTo());
        })->andReturnTrue();

        $event = new RentSaved($rent);
        $handler->handle($event);
    }

    public function testThrowException()
    {
        $this->expectException(ConfirmationMailNotSentException::class);

        $email = 'yannick@email.fr';
        $this->swiftMailer->shouldReceive('send')->once()->withArgs(function (\Swift_Message $mail) use ($email){
            return array_key_exists($email, $mail->getTo());
        })->andThrow(new Swift_TransportException('Error'));

        $handler = new SendEmailConfirmationOnRentSaved();

        $rent = Rent::fromArray([
            'name' => 'yannick',
            'email' => $email,
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('2020-01-01'),
            'endDate' => new CarbonImmutable('2020-01-31')
        ]);
        $event = new RentSaved($rent);
        $handler->handle($event);
    }
}
