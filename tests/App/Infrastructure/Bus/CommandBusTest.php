<?php

namespace App\Infrastructure\Bus;

use App\Commands\Rent\Handlers\MakeARentCommandHandler;
use App\Commands\Rent\MakeARentCommand;
use App\CreatesApplication;
use App\Domain\Rent\Rent;
use App\Exceptions\InfrastructureException;
use App\Repositories\InfrastructureExceptionRepository;
use Carbon\CarbonImmutable;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CommandBusTest extends TestCase
{
    use CreatesApplication;

    private Application $application;
    private Factory $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->application = $this->createApplication();
        $this->factory = $this->createMock(Factory::class);
        DB::table('infrastructure_exceptions')->truncate();
    }

    public function testCreateBus()
    {
        $handler = $this->createMock(MakeARentCommandHandler::class);
        $handler->expects(self::once())->method('listen')->willReturn(MakeARentCommand::class);
        new CommandBus([$handler]);
    }

    public function testDispatch()
    {
        $handler = $this->createMock(MakeARentCommandHandler::class);
        $handler->expects(self::once())->method('listen')->willReturn(MakeARentCommand::class);
        $bus = new CommandBus([$handler]);

        $handler->expects(self::once())->method('handle');
        $bus->dispatch(new MakeARentCommand($rent = Rent::fromArray([
            'name' => 'yannick',
            'email' => 'yannick@email.fr',
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('2020-01-01'),
            'endDate' => new CarbonImmutable('2020-01-31')
        ])));
    }

    public function testThrowExceptionOnUnhandledCommand()
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('Handler for command App\Commands\Rent\MakeARentCommand not found');
        $bus = new CommandBus([]);
        $bus->dispatch(new MakeARentCommand($rent = Rent::fromArray([
            'name' => 'yannick',
            'email' => 'yannick@email.fr',
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('2020-01-01'),
            'endDate' => new CarbonImmutable('2020-01-31')
        ])));
    }

    public function testReportException()
    {
        $handler = $this->createMock(MakeARentCommandHandler::class);
        $handler->expects(self::once())->method('listen')->willReturn(MakeARentCommand::class);
        $bus = new CommandBus([$handler]);

        $handler->expects(self::once())->method('handle')
            ->willThrowException(new InfrastructureException('UseCase', 'Exception'));
        $bus->dispatch(new MakeARentCommand($rent = Rent::fromArray([
            'name' => 'yannick',
            'email' => 'yannick@email.fr',
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('2020-01-01'),
            'endDate' => new CarbonImmutable('2020-01-31')
        ])));

        $repository = new InfrastructureExceptionRepository($this->application, $this->factory);
        self::assertCount(1, $repository->all());
    }
}
