<?php

namespace App\Infrastructure\Bus;

use App\Queries\Bike\FetchAllBikesQuery;
use App\Queries\Bike\Handlers\FetchAllBikesQueryHandler;
use App\Queries\Rent\FetchAllRentsQuery;
use PHPUnit\Framework\TestCase;

class QueryBusTest extends TestCase
{
    public function testCreateBus()
    {
        $handler = $this->createMock(FetchAllBikesQueryHandler::class);
        $handler->expects(self::once())->method('listen')->willReturn(FetchAllBikesQuery::class);
        new QueryBus([$handler]);
    }

    public function testDispatch()
    {
        $handler = $this->createMock(FetchAllBikesQueryHandler::class);
        $handler->expects(self::once())->method('listen')->willReturn(FetchAllBikesQuery::class);
        $bus = new QueryBus([$handler]);

        $handler->expects(self::once())->method('handle');
        $bus->dispatch(new FetchAllBikesQuery());
    }

    public function testThrowExceptionOnUnhandledCommand()
    {
        $this->expectException(\LogicException::class);
        $handler = $this->createMock(FetchAllBikesQueryHandler::class);
        $handler->expects(self::once())->method('listen')->willReturn(FetchAllBikesQuery::class);
        $bus = new QueryBus([$handler]);

        $bus->dispatch(new FetchAllRentsQuery());
    }

}
