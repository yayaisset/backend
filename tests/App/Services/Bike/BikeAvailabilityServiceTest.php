<?php

namespace App\Services\Bike;

use App\Domain\Bike\Bike;
use App\Domain\Rent\RentPeriod;
use App\Repositories\Contracts\BikeRepositoryInterface;
use App\Repositories\Contracts\RentRepositoryInterface;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;

class BikeAvailabilityServiceTest extends TestCase
{
    private $bikeRepository;
    private $rentRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bikeRepository = $this->createMock(BikeRepositoryInterface::class);
        $this->rentRepository = $this->createMock(RentRepositoryInterface::class);
    }

    public function testBikeIsAvailable()
    {
        $bike = Bike::fromArray(['manufacturer' => 'Yamaha', 'category' => 'Superbike', 'stock' => '5']);
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));

        $this->bikeRepository->expects(self::once())->method('findBikeById')->with($bike->id)->willReturn($bike);
        $this->rentRepository->expects(self::once())->method('countRentsByBikeAndPeriod')->with($bike->id, $period)->willReturn(0);

        $service = new BikeAvailabilityService($this->bikeRepository, $this->rentRepository);
        self::assertTrue($service->isBikeAvailableForAPeriod($bike->id, $period));
    }

    public function testBikeIsAvailableWithRentsOnPeriod()
    {
        $bike = Bike::fromArray(['manufacturer' => 'Honda', 'category' => 'MotoGp', 'stock' => '5']);
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));

        $this->bikeRepository->expects(self::once())->method('findBikeById')->with($bike->id)->willReturn($bike);
        $this->rentRepository->expects(self::once())->method('countRentsByBikeAndPeriod')->with($bike->id, $period)->willReturn(2);

        $service = new BikeAvailabilityService($this->bikeRepository, $this->rentRepository);
        self::assertTrue($service->isBikeAvailableForAPeriod($bike->id, $period));
    }

    public function testBikeIsUnavailable()
    {
        $bike = Bike::fromArray(['manufacturer' => 'Suzuki', 'category' => 'Superbike', 'stock' => '0']);
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));

        $this->bikeRepository->expects(self::once())->method('findBikeById')->with($bike->id)->willReturn($bike);
        $this->rentRepository->expects(self::once())->method('countRentsByBikeAndPeriod')->with($bike->id,
            $period)->willReturn(0);

        $service = new BikeAvailabilityService($this->bikeRepository, $this->rentRepository);
        self::assertTrue($service->isBikeUnavailableForAPeriod($bike->id, $period));
    }

    public function testBikeIsUnavailableWithRentsOnPeriod()
    {
        $bike = Bike::fromArray(['manufacturer' => 'Ktm', 'category' => 'MotoGp', 'stock' => '3']);
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));

        $this->bikeRepository->expects(self::once())->method('findBikeById')->with($bike->id)->willReturn($bike);
        $this->rentRepository->expects(self::once())->method('countRentsByBikeAndPeriod')->with($bike->id, $period)->willReturn(3);

        $service = new BikeAvailabilityService($this->bikeRepository, $this->rentRepository);
        self::assertTrue($service->isBikeUnavailableForAPeriod($bike->id, $period));
    }

    public function testGetBikeWithAvailability()
    {
        $bike = Bike::fromArray(['manufacturer' => 'Honda', 'category' => 'MotoGp', 'stock' => '5']);
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));

        $this->bikeRepository->expects(self::once())->method('findBikeById')->with($bike->id)->willReturn($bike);
        $this->rentRepository->expects(self::once())->method('countRentsByBikeAndPeriod')->with($bike->id, $period)->willReturn(2);

        $service = new BikeAvailabilityService($this->bikeRepository, $this->rentRepository);
        $expectedBike = clone $bike;
        $expectedBike->stock = 3;
        self::assertEquals($expectedBike, $service->getBikeWithAvailabilityForAPeriod($bike->id, $period));
    }
}
