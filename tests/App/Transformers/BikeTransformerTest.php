<?php

namespace App\Transformers;

use App\Domain\Bike\Bike;
use PHPUnit\Framework\TestCase;

class BikeTransformerTest extends TestCase
{
    public function testTransform()
    {
        $expectedValues = ['category' => 'Superbike', 'manufacturer' => 'Suzuki', 'stock' => 5];
        $bike = Bike::fromArray($expectedValues);

        $bikeTransformer = new BikeTransformer();

        $actualValues = $bikeTransformer->transform($bike);
        self::assertEquals($expectedValues['category'], $actualValues['category']);
        self::assertEquals($expectedValues['manufacturer'], $actualValues['manufacturer']);
        self::assertEquals($expectedValues['stock'], $actualValues['stock']);
    }

}
