<?php

namespace App\Transformers;

use App\Domain\Rent\Rent;
use App\Models\RentModel;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class RentTransformerTest extends TestCase
{
    public function testTransform()
    {
        $expectedValues = ['name' => 'yannick',
                           'email' => 'yannick@email.fr',
                           'bike' => Uuid::uuid4()->toString(),
                           'startDate' => new CarbonImmutable('2020-01-01'),
                           'endDate' => new CarbonImmutable('2020-01-31')];
        $rent = Rent::fromArray($expectedValues);

        $bikeTransformer = new RentTransformer();

        $actualValues = $bikeTransformer->transform($rent);
        self::assertEquals($expectedValues['name'], $actualValues['name']);
        self::assertEquals($expectedValues['email'], $actualValues['email']);
        self::assertEquals($expectedValues['bike'], $actualValues['bike']);
        self::assertEquals($expectedValues['startDate']->format('d/m/Y'), $actualValues['startDate']);
        self::assertEquals($expectedValues['startDate']->format('d/m/Y'), $actualValues['startDate']);
    }
}
