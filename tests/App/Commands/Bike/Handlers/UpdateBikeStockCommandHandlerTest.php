<?php

namespace App\Commands\Bike\Handlers;

use App\Commands\Bike\UpdateBikeStockCommand;
use App\Repositories\Contracts\BikeRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class UpdateBikeStockCommandHandlerTest extends TestCase
{
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(BikeRepositoryInterface::class);
    }


    public function testUpdateBikeStock()
    {
        $bikeId = Uuid::uuid4();
        $stock = 10;
        $this->repository->expects(self::once())->method('updateBike')->with($bikeId, ["stock"=>$stock]);

        $handler = new UpdateBikeStockCommandHandler($this->repository);
        $handler->handle(new UpdateBikeStockCommand($bikeId, $stock));
    }

    public function testListen()
    {
        $handler = new UpdateBikeStockCommandHandler($this->repository);
        self::assertEquals(UpdateBikeStockCommand::class, $handler->listen());
    }
}
