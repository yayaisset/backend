<?php

namespace App\Commands\Bike\Handlers;

use App\Commands\Bike\CreateBikeCommand;
use App\Domain\Bike\Bike;
use App\Repositories\Contracts\BikeRepositoryInterface;
use PHPUnit\Framework\TestCase;

class CreateBikeCommandHandlerTest extends TestCase
{
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(BikeRepositoryInterface::class);
    }


    public function testHandle()
    {
        $bike = Bike::fromArray(['manufacturer'=> 'Suzuki', 'category'=>'MotoGp', 'stock'=>'5']);

        $this->repository->expects(self::once())->method('createBike');

        $handler = new CreateBikeCommandHandler($this->repository);
        $handler->handle(new CreateBikeCommand($bike));
    }

    public function testListen()
    {
        $handler = new CreateBikeCommandHandler($this->repository);
        self::assertEquals(CreateBikeCommand::class, $handler->listen());
    }
}
