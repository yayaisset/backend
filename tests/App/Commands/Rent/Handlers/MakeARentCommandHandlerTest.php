<?php

namespace App\Commands\Rent\Handlers;

use App\Commands\Rent\MakeARentCommand;
use App\CreatesApplication;
use App\Domain\Rent\Rent;
use App\Events\Rent\RentSaved;
use App\Exceptions\BusinessException;
use App\Repositories\Contracts\RentRepositoryInterface;
use App\Services\Bike\Contracts\BikeAvailabilityServiceInterface;
use App\TestCase;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Event;
use Ramsey\Uuid\Uuid;


class MakeARentCommandHandlerTest extends TestCase
{
    use CreatesApplication;

    private $repository;
    private $bikeAvailability;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(RentRepositoryInterface::class);
        $this->bikeAvailability = $this->createMock(BikeAvailabilityServiceInterface::class);
    }


    public function testSaveRent()
    {
        $rent = Rent::fromArray([
            'name' => 'aName',
            'email' => 'anEmail',
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable('now'),
            'endDate' => new CarbonImmutable('tomorrow')
        ]);

        $this->bikeAvailability->expects(self::once())->method('isBikeUnavailableForAPeriod')->with($rent->bike, $rent->rentPeriod)->willReturn(false);
        $this->repository->expects(self::once())->method('createRent');

        $handler = new MakeARentCommandHandler($this->repository, $this->bikeAvailability);
        Event::fake();
        $handler->handle(new MakeARentCommand($rent));
        Event::assertDispatched(RentSaved::class);
    }

    public function testThrowExceptionOnBikeUnavailable()
    {
        $this->expectException(BusinessException::class);
        $this->expectExceptionMessage('Sorry, the bike is unavailable');
        $rent = Rent::fromArray([
            'name' => 'aName',
            'email' => 'anEmail',
            'bike' => Uuid::uuid4(),
            'startDate' => new CarbonImmutable(),
            'endDate' => new CarbonImmutable()
        ]);

        $this->bikeAvailability->expects(self::once())->method('isBikeUnavailableForAPeriod')->with($rent->bike, $rent->rentPeriod)->willReturn(true);
        $this->repository->expects(self::never())->method('createRent');

        $handler = new MakeARentCommandHandler($this->repository, $this->bikeAvailability);
        Event::fake();
        $handler->handle(new MakeARentCommand($rent));
        Event::assertNotDispatched(RentSaved::class);
    }

    public function testListen()
    {
        $handler = new MakeARentCommandHandler($this->repository, $this->bikeAvailability);
        self::assertEquals(MakeARentCommand::class, $handler->listen());
    }
}
