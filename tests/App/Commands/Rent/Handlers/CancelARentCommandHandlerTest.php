<?php
namespace App\Commands\Rent\Handlers;

use App\Commands\Rent\CancelARentCommand;
use App\Repositories\Contracts\RentRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CancelARentCommandHandlerTest extends TestCase
{
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->createMock(RentRepositoryInterface::class);
    }


    public function testCancelARent()
    {
        $rentId = Uuid::uuid4();
        $this->repository->expects(self::once())->method('deleteRent')->with($rentId);
        $handler = new CancelARentCommandHandler($this->repository);
        $handler->handle(new CancelARentCommand($rentId));
    }


    public function testListen()
    {
        $handler = new CancelARentCommandHandler($this->repository);
        self::assertEquals(CancelARentCommand::class, $handler->listen());
    }
}
