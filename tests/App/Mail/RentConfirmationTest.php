<?php

namespace App\Mail;

use PHPUnit\Framework\TestCase;

class RentConfirmationTest extends TestCase
{
    public function testBuild()
    {
        $rentConfirmation = (new RentConfirmation())->build();
        self::assertEquals('emails.rent.confirmation', $rentConfirmation->getMarkdown());
}
}
