<?php

namespace App\Domain\Rent;

use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;

class RentPeriodTest extends TestCase
{
    public function testCreateRentPeriod()
    {
        $startDate = new CarbonImmutable('2020-01-01');
        $endDate = new CarbonImmutable('2020-01-31');
        $rentPeriod = RentPeriod::fromDates($startDate, $endDate);
        self::assertEquals($startDate, $rentPeriod->getStartDate());
        self::assertEquals($endDate, $rentPeriod->getEndDate());
    }

    public function testThrownExceptionOnInvalidDates()
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('StartDate must be before EndDate');
        $startDate = new CarbonImmutable('2020-01-31');
        $endDate = new CarbonImmutable('2020-01-01');
        $rentPeriod = RentPeriod::fromDates($startDate, $endDate);
    }
}
