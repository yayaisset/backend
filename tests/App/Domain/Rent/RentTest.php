<?php

namespace App\Domain\Rent;

use App\Exceptions\BusinessException;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class RentTest extends TestCase
{
    public function testFromArray()
    {
        $bikeId = Uuid::uuid4();
        $name = 'aName';
        $email = 'address@email.fr';
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));
        $values = [
            'name' => $name,
            'email' => $email,
            'bike' => $bikeId,
            'startDate' => $period->getStartDate()->format('Y-m-d'),
            'endDate' => $period->getEndDate()->format('Y-m-d')
        ];
        $rent = Rent::fromArray($values);

        self::assertEquals($bikeId, $rent->bike);
        self::assertEquals($name, $rent->name);
        self::assertEquals($email, $rent->email);
        self::assertEquals($period, $rent->rentPeriod);
    }

    public function testThrowExceptionOnIncompleteData()
    {
        $this->expectException(BusinessException::class);
        $this->expectExceptionMessage('An error occurred creating your rent, we are so sorry');
        $name = 'aName';
        $email = 'address@email.fr';
        $period = RentPeriod::fromDates(new CarbonImmutable('2020-01-01'), new CarbonImmutable('2020-01-31'));
        $values = [
            'name' => $name,
            'email' => $email,
            'startDate' => $period->getStartDate()->format('Y-m-d'),
            'endDate' => $period->getEndDate()->format('Y-m-d')
        ];
        Rent::fromArray($values);

    }
}
