<?php

namespace App\Domain\Bike;

use App\Exceptions\BusinessException;
use PHPUnit\Framework\TestCase;

class BikeTest extends TestCase
{
    public function testFromArray()
    {
        $category = 'Superbike';
        $manufacturer = 'Honda';
        $stock = 5;
        $values = ['category' => $category, 'manufacturer' => $manufacturer, 'stock' => $stock];
        $bike = Bike::fromArray($values);

        self::assertEquals($category, $bike->category->getValue());
        self::assertEquals($manufacturer, $bike->manufacturer->getValue());
        self::assertEquals($stock, $bike->stock);
    }

    public function testThrowExceptionOnInvalidData()
    {
        $this->expectException(BusinessException::class);
        $this->expectExceptionMessage('An error occurred, please contact your administrator ("Moto3" is not an acceptable value for "App\Domain\Bike\BikeCategory" enum.)');
        $category = 'Moto3';
        $manufacturer = 'Honda';
        $stock = 5;
        $values = ['category' => $category, 'manufacturer' => $manufacturer, 'stock' => $stock];
        Bike::fromArray($values);

    }

    public function testThrowExceptionOnIncompleteData()
    {
        $this->expectException(BusinessException::class);
        $this->expectExceptionMessage('An error occurred, please contact your administrator (Undefined index: manufacturer');
        $category = 'MotoGp';
        $stock = 5;
        $values = ['category' => $category, 'stock' => $stock];
        Bike::fromArray($values);
    }
}
