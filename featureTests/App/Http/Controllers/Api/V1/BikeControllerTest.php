<?php

namespace App\Http\Controllers\Api\V1;

use A;
use App\Commands\Bike\CreateBikeCommand;
use App\Commands\Bike\UpdateBikeStockCommand;
use App\Domain\Bike\Bike;
use App\Domain\Rent\RentPeriod;
use App\Infrastructure\Bus\CommandBus;
use App\Infrastructure\Bus\Contracts\CommandBusInterface;
use App\Infrastructure\Bus\Contracts\QueryBusInterface;
use App\Infrastructure\Bus\QueryBus;
use App\Queries\Bike\FetchAllAvailableBikesForAPeriodQuery;
use App\Queries\Bike\FetchAllBikesQuery;
use App\TestCase;
use Carbon\CarbonImmutable;
use Illuminate\Support\Collection;
use Ramsey\Uuid\Uuid;


class BikeControllerTest extends TestCase
{
    private QueryBus $queryBus;
    private CommandBus $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->createMock(QueryBus::class);
        $this->app->instance(QueryBusInterface::class, $this->queryBus);
        $this->commandBus = $this->createMock(CommandBus::class);
        $this->app->instance(CommandBusInterface::class, $this->commandBus);
    }


    public function testFetchAllBikes()
    {
        $bike1 = Bike::fromArray(['manufacturer'=> 'Honda', 'category'=>'Superbike', 'stock'=>'5']);
        $bike2 = Bike::fromArray(['manufacturer'=> 'Yamaha', 'category'=>'MotoGp', 'stock'=>'5']);
        $this->queryBus->expects(self::once())->method('dispatch')->with(new FetchAllBikesQuery())->willReturn(Collection::make([
            $bike1,
            $bike2
        ]));

        $response = $this->get('/api/bikes');

        $response->assertStatus(200);
        /** @var Collection $originalContent */
        $originalContent = $response->getOriginalContent();
        self::assertCount(2, $originalContent);
    }

    public function testFetchAllAvailableBikes()
    {
        $bike1 = Bike::fromArray(['manufacturer'=> 'Honda', 'category'=>'Superbike', 'stock'=>'5']);
        $bike2 = Bike::fromArray(['manufacturer'=> 'Yamaha', 'category'=>'MotoGp', 'stock'=>'2']);
        $startDate = '2020-01-01';
        $endDate = '2020-01-31';
        $rentPeriod = RentPeriod::fromDates(new CarbonImmutable($startDate), new CarbonImmutable($endDate));
        $this->queryBus->expects(self::once())->method('dispatch')->with(new FetchAllAvailableBikesForAPeriodQuery($rentPeriod))->willReturn(Collection::make([
            $bike1,
            $bike2
        ]));

        $response = $this->get("/api/bikes/available/start/$startDate/end/$endDate");

        $response->assertStatus(200);
        /** @var Collection $originalContent */
        $originalContent = $response->getOriginalContent();
        self::assertCount(2, $originalContent);
    }

    public function testCreateBike()
    {
        $values = ['category' => 'Superbike', 'manufacturer' => 'Ducati', 'stock' => 5];

        $this->commandBus->expects(self::once())->method('dispatch')
            ->with(static::callback(function (CreateBikeCommand $command) use ($values) {
                return (
                    $command->bike->category->getValue() === $values['category'] &&
                    $command->bike->manufacturer->getValue() === $values['manufacturer'] &&
                    $command->bike->stock === $values['stock']
                );
        }));
        $response = $this->call('POST', '/api/bikes/create', [], [], [], [], json_encode($values));

        $response->assertStatus(201);
    }

    public function testUpdateBikeStock()
    {
        $bikeId = Uuid::uuid4();
        $stock = 10;
        $this->commandBus->expects(self::once())->method('dispatch')
            ->with(static::callback(function (UpdateBikeStockCommand $command) use ($bikeId, $stock) {
                return (
                    $command->bikeId == $bikeId &&
                    $command->stock == $stock
                );
            }));
        $response = $this->call('PUT', "/api/bikes/update/{$bikeId->toString()}/stock/$stock", [], [], [], []);

        $response->assertStatus(200);
    }
}
