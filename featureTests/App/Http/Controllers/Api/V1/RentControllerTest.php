<?php

namespace App\Http\Controllers\Api\V1;

use App\Commands\Rent\CancelARentCommand;
use App\Commands\Rent\MakeARentCommand;
use App\Domain\Rent\Rent;
use App\Infrastructure\Bus\CommandBus;
use App\Infrastructure\Bus\Contracts\CommandBusInterface;
use App\Infrastructure\Bus\Contracts\QueryBusInterface;
use App\Infrastructure\Bus\QueryBus;
use App\Queries\Rent\FetchAllRentsQuery;
use App\TestCase;
use Carbon\CarbonImmutable;
use Illuminate\Support\Collection;
use Ramsey\Uuid\Uuid;

class RentControllerTest extends TestCase
{
    private CommandBus $commandBus;
    private QueryBus $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->createMock(QueryBus::class);
        $this->app->instance(QueryBusInterface::class, $this->queryBus);
        $this->commandBus = $this->createMock(CommandBus::class);
        $this->app->instance(CommandBusInterface::class, $this->commandBus);
    }

    public function testFetchAllRents()
    {
        $rent1 = Rent::fromArray(['name' => 'yannick',
                                       'email' => 'yannick@email.fr',
                                       'bike' => Uuid::uuid4()->toString(),
                                       'startDate' => new CarbonImmutable('2020-01-01'),
                                       'endDate' => new CarbonImmutable('2020-01-31')]);
        $rent2 = Rent::fromArray(['name' => 'gaelle',
                                       'email' => 'gaelle@email.fr',
                                       'bike' => Uuid::uuid4()->toString(),
                                       'startDate' => new CarbonImmutable('2020-02-01'),
                                       'endDate' => new CarbonImmutable('2020-02-28')]);
        $this->queryBus->expects(self::once())->method('dispatch')->with(new FetchAllRentsQuery())->willReturn(Collection::make([
            $rent1,
            $rent2
        ]));

        $response = $this->get('/api/rents');

        $response->assertStatus(200);
        /** @var Collection $originalContent */
        $originalContent = $response->getOriginalContent();
        self::assertCount(2, $originalContent);
    }

    public function testMakeARent()
    {
        $values = ['name' => 'yannick',
                   'email' => 'yannick@email.fr',
                   'bike' => Uuid::uuid4(),
                   'startDate' => new CarbonImmutable('2020-01-01'),
                   'endDate' => new CarbonImmutable('2020-01-31')];

        $this->commandBus->expects(self::once())->method('dispatch')
            ->with(static::callback(function (MakeARentCommand $command) use ($values) {
                return (
                    $command->rent->name === $values['name'] &&
                    $command->rent->email === $values['email'] &&
                    $command->rent->bike == $values['bike'] &&
                    $command->rent->rentPeriod->getStartDate() == $values['startDate'] &&
                    $command->rent->rentPeriod->getEndDate() == $values['endDate']
                );
            }));
        $response = $this->call('POST', '/api/rents/create', [], [], [], [], json_encode($values));

        $response->assertStatus(201);
    }

    public function testCancelARent()
    {
        $rentId = Uuid::uuid4();
        $this->commandBus->expects(self::once())->method('dispatch')
            ->with(static::callback(function (CancelARentCommand $command) use ($rentId) {
                return $command->rentId == $rentId ;
            }));
        $response = $this->call('DELETE', "/api/rents/delete/{$rentId->toString()}", [], [], [], []);

        $response->assertStatus(200);
    }
}
