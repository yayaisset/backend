<?php

use Dingo\Api\Routing\Router;

$api = app(Router::class);

$api->version('v1', [], function (Router $api) {
    $api->get('bikes', 'App\Http\Controllers\Api\V1\BikeController@fetchAllBikes');
    $api->get('bikes/available/start/{startDate}/end/{endDate}', 'App\Http\Controllers\Api\V1\BikeController@fetchAllAvailableBikesForAPeriod');
    $api->post('bikes/create', 'App\Http\Controllers\Api\V1\BikeController@createBike');
    $api->put('bikes/update/{bikeId}/stock/{stock}', 'App\Http\Controllers\Api\V1\BikeController@updateBikeStock');

    $api->get('rents', 'App\Http\Controllers\Api\V1\RentController@fetchAllRents');
    $api->post('rents/create', 'App\Http\Controllers\Api\V1\RentController@makeARent');
    $api->delete('rents/delete/{rentId}', 'App\Http\Controllers\Api\V1\RentController@cancelARent');
});
