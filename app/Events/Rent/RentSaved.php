<?php

namespace App\Events\Rent;

use App\Domain\Rent\Rent;
use App\Models\RentModel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RentSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Rent $rent;

    public function __construct(Rent $rent)
    {
        $this->rent = $rent;
    }
}
