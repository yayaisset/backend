<?php

namespace App\Events\Rent\Handlers;

use App\Events\Rent\RentSaved;
use App\Exceptions\ConfirmationMailNotSentException;
use App\Mail\RentConfirmation;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Swift_TransportException;

class SendEmailConfirmationOnRentSaved
{

    public function handle(RentSaved $event)
    {
        try {
            Mail::to($event->rent->email)->send(new RentConfirmation());
        } catch (Swift_TransportException $e) {
            throw new ConfirmationMailNotSentException(__CLASS__, $e);
        }
    }
}
