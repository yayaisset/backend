<?php

namespace App\Providers;

use App\Infrastructure\Bus\CommandBus;
use App\Infrastructure\Bus\Contracts\CommandBusInterface;
use App\Infrastructure\Bus\Contracts\QueryBusInterface;
use App\Infrastructure\Bus\QueryBus;
use App\Repositories\BikeRepository;
use App\Repositories\Contracts\BikeRepositoryInterface;
use App\Repositories\Contracts\RentRepositoryInterface;
use App\Repositories\RentRepository;
use App\Services\Bike\BikeAvailabilityService;
use App\Services\Bike\Contracts\BikeAvailabilityServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register Services
        //@TODO Make services registration automatic
        $this->app->bind(BikeAvailabilityServiceInterface::class, BikeAvailabilityService::class);

        // Register Repositories
        //@TODO Make repositories registration automatic
        $this->app->bind(BikeRepositoryInterface::class, BikeRepository::class);
        $this->app->bind(RentRepositoryInterface::class, RentRepository::class);

        $this->app->bind(QueryBusInterface::class, fn($app) => new QueryBus($app->tagged('query_handler')));
        $this->app->bind(CommandBusInterface::class, fn($app) => new CommandBus($app->tagged('command_handler')));

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


}
