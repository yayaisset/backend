<?php

namespace App\Providers;


use App\Commands\Bike\CreateBikeCommand;
use App\Commands\Bike\Handlers\CreateBikeCommandHandler;
use App\Commands\Bike\Handlers\UpdateBikeStockCommandHandler;
use App\Commands\Bike\UpdateBikeStockCommand;
use App\Commands\Rent\CancelARentCommand;
use App\Commands\Rent\Handlers\CancelARentCommandHandler;
use App\Commands\Rent\Handlers\MakeARentCommandHandler;
use App\Commands\Rent\MakeARentCommand;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{

    //@TODO Make commands registration automatic
    protected $listen = [
        CreateBikeCommand::class => [
            CreateBikeCommandHandler::class,
        ],
        UpdateBikeStockCommand::class => [
            UpdateBikeStockCommandHandler::class,
        ],

        MakeARentCommand::class => [
            MakeARentCommandHandler::class,
        ],
        CancelARentCommand::class => [
            CancelARentCommandHandler::class,
        ],
    ];

    public function register()
    {
        parent::register();
        foreach ($this->listen as $handler) {
            $this->app->tag($handler, ['command_handler']);
        }
    }
}
