<?php
namespace App\Providers;


use App\Queries\Bike\FetchAllAvailableBikesForAPeriodQuery;
use App\Queries\Bike\FetchAllBikesQuery;
use App\Queries\Bike\Handlers\FetchAllAvailableBikesForAPeriodQueryHandler;
use App\Queries\Bike\Handlers\FetchAllBikesQueryHandler;
use App\Queries\Rent\FetchAllRentsQuery;
use App\Queries\Rent\Handlers\FetchAllRentsQueryHandler;
use Illuminate\Support\ServiceProvider;

class QueryServiceProvider extends ServiceProvider
{
    //@TODO Make queries registration automatic
    protected $listen = [
        FetchAllBikesQuery::class => [
            FetchAllBikesQueryHandler::class,
        ],
        FetchAllAvailableBikesForAPeriodQuery::class => [
            FetchAllAvailableBikesForAPeriodQueryHandler::class,
        ],

        FetchAllRentsQuery::class => [
            FetchAllRentsQueryHandler::class,
        ],
    ];

    public function register()
    {
        parent::register();
        foreach ($this->listen as $handler) {
            $this->app->tag($handler, ['query_handler']);
        }
    }
}
