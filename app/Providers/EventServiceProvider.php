<?php

namespace App\Providers;

use App\Events\Rent\Handlers\SendEmailConfirmationOnRentSaved;
use App\Events\Rent\RentSaved;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RentSaved::class => [
            SendEmailConfirmationOnRentSaved::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
