<?php

namespace App\Domain\Rent;


use Carbon\CarbonInterface;

class RentPeriod
{

    private CarbonInterface $startDate;
    private CarbonInterface $endDate;

    private function __construct(CarbonInterface $startDate, CarbonInterface $endDate)
    {
        if ($startDate > $endDate)
            throw new \LogicException('StartDate must be before EndDate');
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public static function fromDates(CarbonInterface $startDate, CarbonInterface $endDate): self
    {
        return new self($startDate, $endDate);
    }


    public function getStartDate(): CarbonInterface
    {
        return $this->startDate;
    }

    public function getEndDate(): CarbonInterface
    {
        return $this->endDate;
    }


}
