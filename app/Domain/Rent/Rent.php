<?php
namespace App\Domain\Rent;


use App\Exceptions\BusinessException;
use App\Models\Mappers\RentMapper;
use Carbon\CarbonImmutable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Rent
{
    use RentMapper;

    public UuidInterface $id;
    public string $name;
    public string $email;
    public UuidInterface $bike;
    public RentPeriod $rentPeriod;

    public static function fromArray(array $values): self
    {
        $instance = new self();
        try {
            $instance->id = Uuid::uuid4();
            $instance->name = $values['name'];
            $instance->email = $values['email'];
            $instance->bike = Uuid::fromString($values['bike']);
            $instance->rentPeriod = RentPeriod::fromDates(new CarbonImmutable($values['startDate']),
                new CarbonImmutable($values['endDate']));
        } catch (\Exception $e) {
            throw new BusinessException('An error occurred creating your rent, we are so sorry');
        }
        return $instance;
    }
}
