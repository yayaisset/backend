<?php

namespace App\Domain\Bike;


use Elao\Enum\Enum;

class BikeCategory extends Enum
{
    const MOTOGP = 'MotoGp';
    const SUPERBIKE = 'Superbike';

    public static function values(): array
    {
        return [
            self::MOTOGP,
            self:: SUPERBIKE
        ];
    }
}
