<?php
namespace App\Domain\Bike;


use App\Exceptions\BusinessException;
use App\Models\Mappers\BikeMapper;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Bike
{
    use BikeMapper;

    public UuidInterface $id;
    public BikeCategory $category;
    public BikeManufacturer $manufacturer;
    public int $stock;

    public static function fromArray($values): self
    {
        $instance = new self();
        try {
            $instance->id = Uuid::uuid4();
            $instance->category = BikeCategory::get($values['category']);
            $instance->manufacturer = BikeManufacturer::get($values['manufacturer']);
            $instance->stock = $values['stock'];
        } catch (\Exception $e) {
            throw new BusinessException("An error occurred, please contact your administrator ({$e->getMessage()})");
        }

        return $instance;
    }

}
