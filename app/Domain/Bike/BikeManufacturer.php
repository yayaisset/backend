<?php

namespace App\Domain\Bike;


use Elao\Enum\Enum;

class BikeManufacturer extends Enum
{

    const SUZUKI = 'Suzuki';
    const HONDA = 'Honda';
    const YAMAHA = 'Yamaha';
    const DUCATI = 'Ducati';
    const KTM = 'Ktm';

    public static function values(): array
    {
        return [
            self::SUZUKI,
            self::HONDA,
            self::YAMAHA,
            self::DUCATI,
            self::KTM
        ];
    }
}
