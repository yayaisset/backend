<?php
namespace App\Commands\Rent;

use App\Domain\Rent\Rent;
use App\Infrastructure\Command\Contracts\CommandInterface;

class MakeARentCommand implements CommandInterface
{
    public Rent $rent;

    public function __construct(Rent $rent)
    {
        $this->rent = $rent;
    }
}
