<?php

namespace App\Commands\Rent;


use App\Infrastructure\Command\Contracts\CommandInterface;
use Ramsey\Uuid\UuidInterface;

class CancelARentCommand implements CommandInterface
{
    public UuidInterface $rentId;

    public function __construct(UuidInterface $rentId)
    {
        $this->rentId = $rentId;
    }
}
