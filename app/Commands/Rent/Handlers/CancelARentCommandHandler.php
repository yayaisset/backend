<?php
namespace App\Commands\Rent\Handlers;


use App\Commands\Rent\CancelARentCommand;
use App\Infrastructure\Command\Contracts\CommandHandlerInterface;
use App\Infrastructure\Command\Contracts\CommandInterface;
use App\Repositories\Contracts\RentRepositoryInterface;

class CancelARentCommandHandler implements CommandHandlerInterface
{

    private RentRepositoryInterface $repository;

    public function __construct(RentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CommandInterface|CancelARentCommand $command
     */
    public function handle(CommandInterface $command)
    {
        $this->repository->deleteRent($command->rentId);
    }

    public function listen(): string
    {
        return CancelARentCommand::class;
    }
}
