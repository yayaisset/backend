<?php
namespace App\Commands\Rent\Handlers;

use App\Commands\Rent\MakeARentCommand;
use App\Events\Rent\RentSaved;
use App\Exceptions\BusinessException;
use App\Exceptions\InfrastructureException;
use App\Infrastructure\Command\Contracts\CommandHandlerInterface;
use App\Infrastructure\Command\Contracts\CommandInterface;
use App\Repositories\Contracts\RentRepositoryInterface;
use App\Services\Bike\Contracts\BikeAvailabilityServiceInterface;

class MakeARentCommandHandler implements CommandHandlerInterface
{
    private RentRepositoryInterface $repository;
    private BikeAvailabilityServiceInterface $bikeAvailability;

    public function __construct(RentRepositoryInterface $repository, BikeAvailabilityServiceInterface $bikeAvailability)
    {
        $this->repository = $repository;
        $this->bikeAvailability = $bikeAvailability;
    }

    /**
     * @param CommandInterface|MakeARentCommand $command
     * @throws InfrastructureException
     */
    public function handle(CommandInterface $command)
    {
        if ($this->bikeAvailability->isBikeUnavailableForAPeriod($command->rent->bike, $command->rent->rentPeriod))
            throw new BusinessException('Sorry, the bike is unavailable');

        $this->repository->createRent($command->rent);
        // Rent is saved, send notification event
        event(new RentSaved($command->rent));
    }

    public function listen(): string
    {
        return MakeARentCommand::class;
    }
}
