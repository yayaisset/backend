<?php

namespace App\Commands\Bike;


use App\Infrastructure\Command\Contracts\CommandInterface;
use Ramsey\Uuid\UuidInterface;

class UpdateBikeStockCommand implements CommandInterface
{
    public UuidInterface $bikeId;
    public int $stock;

    public function __construct(UuidInterface $bikeId, int $stock)
    {
        $this->bikeId = $bikeId;
        $this->stock = $stock;
    }
}
