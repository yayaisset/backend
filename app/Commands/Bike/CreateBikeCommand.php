<?php
namespace App\Commands\Bike;

use App\Domain\Bike\Bike;
use App\Infrastructure\Command\Contracts\CommandInterface;

class CreateBikeCommand implements CommandInterface
{
    public Bike $bike;

    public function __construct(Bike $bike)
    {
        $this->bike = $bike;
    }
}
