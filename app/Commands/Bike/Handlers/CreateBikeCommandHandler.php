<?php
namespace App\Commands\Bike\Handlers;

use App\Commands\Bike\CreateBikeCommand;
use App\Infrastructure\Command\Contracts\CommandHandlerInterface;
use App\Infrastructure\Command\Contracts\CommandInterface;
use App\Repositories\Contracts\BikeRepositoryInterface;

class CreateBikeCommandHandler implements CommandHandlerInterface
{
    private BikeRepositoryInterface $repository;

    public function __construct(BikeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CommandInterface|CreateBikeCommand $command
     */
    public function handle(CommandInterface $command)
    {
        $this->repository->createBike($command->bike);
    }

    public function listen(): string
    {
        return CreateBikeCommand::class;
    }
}
