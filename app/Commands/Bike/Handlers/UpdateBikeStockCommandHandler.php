<?php

namespace App\Commands\Bike\Handlers;


use App\Commands\Bike\UpdateBikeStockCommand;
use App\Infrastructure\Command\Contracts\CommandHandlerInterface;
use App\Infrastructure\Command\Contracts\CommandInterface;
use App\Repositories\Contracts\BikeRepositoryInterface;

class UpdateBikeStockCommandHandler implements CommandHandlerInterface
{
    private BikeRepositoryInterface $repository;

    public function __construct(BikeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param CommandInterface|UpdateBikeStockCommand $command
     */
    public function handle(CommandInterface $command)
    {
        $this->repository->updateBike($command->bikeId, ['stock'=>$command->stock]);
    }

    public function listen(): string
    {
        return UpdateBikeStockCommand::class;
    }
}
