<?php

namespace App\Exceptions;


use App\Models\InfrastructureExceptionModel;
use App\Repositories\Contracts\InfrastructureExceptionRepositoryInterface;
use Carbon\CarbonImmutable;

class InfrastructureException extends \Exception
{
    private string $useCase;

    public function __construct(string $useCase, string $message)
    {
        parent::__construct($message);
        $this->useCase = $useCase;
        $this->message = $message;
    }

    public function report(InfrastructureExceptionRepositoryInterface $repository)
    {
         $repository->storeException(InfrastructureExceptionModel::fromArray([
             'title'=> static::class,
             'message' => $this->message,
             'useCase' => $this->useCase,
             'throwedAt' => new CarbonImmutable()
             ]));
    }
}
