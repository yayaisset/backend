<?php

namespace App\Exceptions;

use App\Repositories\Contracts\InfrastructureExceptionRepositoryInterface;
use App\Repositories\InfrastructureExceptionRepository;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{


    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        BusinessException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->container->bind(InfrastructureExceptionRepositoryInterface::class, InfrastructureExceptionRepository::class);
    }
}
