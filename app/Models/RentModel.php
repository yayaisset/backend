<?php

namespace App\Models;

use App\Models\Mappers\RentMapper;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class RentModel extends Model
{
    use HasFactory, RentMapper;

    protected $table = 'rents';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = ['id', 'name', 'email', 'bike', 'start_date', 'end_date'];

    public function getId()
    {
        return $this->getAttribute('id');
    }

    public function getName(): string
    {
        return $this->getAttribute('name');
    }

    public function getEmail(): string
    {
        return $this->getAttribute('email');
    }

    public function getBikeId(): string
    {
        return $this->getAttribute('bike');
    }

    public function getStartDate(): string
    {
        return $this->getAttribute('start_date');
    }

    public function getEndDate(): string
    {
        return $this->getAttribute('end_date');
    }
}
