<?php

namespace App\Models\Mappers;


use App\Domain\Rent\Rent;
use App\Domain\Rent\RentPeriod;
use App\Models\RentModel;
use Carbon\CarbonImmutable;
use Ramsey\Uuid\Uuid;

trait RentMapper
{
    public static function mapFromModel(RentModel $model): Rent
    {
        $rent = new Rent();
        $rent->id = Uuid::fromString($model->getId());
        $rent->name = $model->getName();
        $rent->email = $model->getEmail();
        $rent->bike = Uuid::fromString($model->getBikeId());
        $rent->rentPeriod = RentPeriod::fromDates(new CarbonImmutable($model->getStartDate()), new CarbonImmutable($model->getEndDate()));
        return $rent;
    }

    public static function mapToModel(Rent $rent): RentModel
    {
        $model = new RentModel();
        $model->setAttribute('id', $rent->id);
        $model->setAttribute('name', $rent->name);
        $model->setAttribute('email', $rent->email);
        $model->setAttribute('bike', $rent->bike);
        $model->setAttribute('start_date', $rent->rentPeriod->getStartDate());
        $model->setAttribute('end_date', $rent->rentPeriod->getEndDate());

        return $model;
    }
}
