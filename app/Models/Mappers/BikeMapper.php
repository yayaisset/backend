<?php

namespace App\Models\Mappers;


use App\Domain\Bike\Bike;
use App\Domain\Bike\BikeCategory;
use App\Domain\Bike\BikeManufacturer;
use App\Models\BikeModel;
use Ramsey\Uuid\Uuid;

trait BikeMapper
{
    public static function mapFromModel(BikeModel $model): Bike
    {
        $bike = new Bike();
        $bike->id = Uuid::fromString($model->getId());
        $bike->category = BikeCategory::get($model->getCategory());
        $bike->manufacturer = BikeManufacturer::get($model->getManufacturer());
        $bike->stock = $model->getStock();
        return $bike;
    }

    public static function mapToModel(Bike $bike): BikeModel
    {
        $model = new BikeModel();
        $model->setAttribute('id', $bike->id);
        $model->setAttribute('category', $bike->category->getValue());
        $model->setAttribute('manufacturer', $bike->manufacturer->getValue());
        $model->setAttribute('stock', $bike->stock);

        return $model;
    }
}
