<?php

namespace App\Models;

use App\Models\Mappers\BikeMapper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BikeModel extends Model
{
    use HasFactory, BikeMapper;

    protected $table = 'bikes';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = ['id', 'category', 'manufacturer', 'stock'];

    public function getId(): string
    {
        return $this->getAttribute('id');
    }

    public function getCategory(): string
    {
        return $this->getAttribute('category');
    }

    public function getManufacturer(): string
    {
        return $this->getAttribute('manufacturer');
    }

    public function getStock(): int
    {
        return (int)$this->getAttribute('stock');
    }
}
