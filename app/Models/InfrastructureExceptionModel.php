<?php

namespace App\Models;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class InfrastructureExceptionModel extends Model
{
    use HasFactory;

    protected $table = 'infrastructure_exceptions';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = ['id', 'title', 'message', 'use_case', 'throwed_at'];

    public static function fromArray(array $values): self
    {
        $instance = new self();
        $instance->setAttribute('id', Uuid::uuid4());
        $instance->setAttribute('title', $values['title']);
        $instance->setAttribute('message', $values['message']);
        $instance->setAttribute('use_case', $values['useCase']);
        $instance->setAttribute('throwed_at', $values['throwedAt']);

        return $instance;
    }

    public function getId(): string
    {
        return $this->getAttribute('id');
    }

    public function getTitle(): string
    {
        return $this->getAttribute('title');
    }

    public function getMessage(): string
    {
        return $this->getAttribute('message');
    }

    public function getUseCase(): string
    {
        return $this->getAttribute('use_case');
    }

    public function getThrowedAt(): CarbonImmutable
    {
        return new CarbonImmutable($this->getAttribute('throwed_at'));
    }

}
