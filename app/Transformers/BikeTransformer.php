<?php
namespace App\Transformers;

use App\Domain\Bike\Bike;
use League\Fractal\TransformerAbstract;


class BikeTransformer extends TransformerAbstract
{
    public function transform(Bike $bike) : array
    {
        return [
            'id' => $bike->id,
            'category' => $bike->category->getValue(),
            'manufacturer' => $bike->manufacturer->getValue(),
            'stock' => $bike->stock
        ];
    }
}
