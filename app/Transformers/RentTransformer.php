<?php
namespace App\Transformers;

use App\Domain\Rent\Rent;
use League\Fractal\TransformerAbstract;


class RentTransformer extends TransformerAbstract
{
    public function transform(Rent $rent) : array
    {
        return [
            'id' => $rent->id,
            'name' => $rent->name,
            'email' => $rent->email,
            'bike' => $rent->bike,
            'startDate' => $rent->rentPeriod->getStartDate()->format('d/m/Y'),
            'endDate' => $rent->rentPeriod->getEndDate()->format('d/m/Y')
        ];
    }
}
