<?php

namespace App\Services\Bike\Contracts;

use App\Domain\Bike\Bike;
use App\Domain\Rent\RentPeriod;
use Ramsey\Uuid\UuidInterface;


interface BikeAvailabilityServiceInterface
{
    public function getBikeWithAvailabilityForAPeriod(UuidInterface $bikeId, RentPeriod $period): Bike;
    public function isBikeAvailableForAPeriod(UuidInterface $bikeId, RentPeriod $period): bool;
    public function isBikeUnavailableForAPeriod(UuidInterface $bikeId, RentPeriod $period): bool;
}
