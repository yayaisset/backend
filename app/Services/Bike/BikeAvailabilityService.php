<?php

namespace App\Services\Bike;

use App\Domain\Bike\Bike;
use App\Domain\Rent\RentPeriod;
use App\Repositories\Contracts\BikeRepositoryInterface;
use App\Repositories\Contracts\RentRepositoryInterface;
use App\Services\Bike\Contracts\BikeAvailabilityServiceInterface;
use Ramsey\Uuid\UuidInterface;


class BikeAvailabilityService implements BikeAvailabilityServiceInterface
{
    /** @var BikeRepositoryInterface */
    private BikeRepositoryInterface $repository;
    private RentRepositoryInterface $rentRepository;

    public function __construct(
        BikeRepositoryInterface $repository,
        RentRepositoryInterface $rentRepository
    )
    {
        $this->repository = $repository;
        $this->rentRepository = $rentRepository;
    }

    public function getBikeWithAvailabilityForAPeriod(UuidInterface $bikeId, RentPeriod $period): Bike
    {
        $bike = $this->repository->findBikeById($bikeId);
        $bike->stock -= $this->rentRepository->countRentsByBikeAndPeriod($bikeId, $period);
        return $bike;
    }

    public function isBikeAvailableForAPeriod(UuidInterface $bikeId, RentPeriod $period): bool
    {
        return $this->getBikeWithAvailabilityForAPeriod($bikeId, $period)->stock > 0;
    }

    public function isBikeUnavailableForAPeriod(UuidInterface $bikeId, RentPeriod $period): bool
    {
        return !$this->isBikeAvailableForAPeriod($bikeId, $period);
    }
}
