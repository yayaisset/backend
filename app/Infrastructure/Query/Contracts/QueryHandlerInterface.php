<?php

namespace App\Infrastructure\Query\Contracts;


interface QueryHandlerInterface
{
    public function handle(QueryInterface $query);

    public function listen(): string;
}
