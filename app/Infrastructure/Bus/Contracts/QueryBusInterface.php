<?php
namespace App\Infrastructure\Bus\Contracts;

use App\Infrastructure\Query\Contracts\QueryInterface;

interface QueryBusInterface
{
    public function dispatch(QueryInterface $query);
}
