<?php
namespace App\Infrastructure\Bus\Contracts;

use App\Infrastructure\Command\Contracts\CommandInterface;

interface CommandBusInterface
{
    public function dispatch(CommandInterface $command);
}
