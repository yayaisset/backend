<?php
namespace App\Infrastructure\Bus;

use App\Exceptions\InfrastructureException;
use App\Infrastructure\Bus\Contracts\CommandBusInterface;
use App\Infrastructure\Command\Contracts\CommandHandlerInterface;
use App\Infrastructure\Command\Contracts\CommandInterface;

class CommandBus implements CommandBusInterface
{
    /**
     * @var CommandHandlerInterface[]
     */
    private array $handlers = [];

    public function __construct(iterable $handlers)
    {
        foreach ($handlers as $handler) {
            $this->handlers[$handler->listen()] = $handler;
        }
    }

    public function dispatch(CommandInterface $command)
    {
        $commandClass = get_class($command);
        if(!array_key_exists($commandClass, $this->handlers)) throw new \LogicException("Handler for command $commandClass not found");
        $handler = $this->handlers[$commandClass];
        try {
            return $handler->handle($command);
        } catch (InfrastructureException $e) {
            report($e);
        }
    }
}
