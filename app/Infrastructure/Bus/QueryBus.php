<?php
namespace App\Infrastructure\Bus;

use App\Infrastructure\Bus\Contracts\QueryBusInterface;
use App\Infrastructure\Query\Contracts\QueryHandlerInterface;
use App\Infrastructure\Query\Contracts\QueryInterface;

class QueryBus implements QueryBusInterface
{
    /**
     * @var QueryHandlerInterface[]
     */
    private $handlers;

    public function __construct(iterable $handlers)
    {

        foreach ($handlers as $handler) {
            $this->handlers[$handler->listen()] = $handler;
        }
    }

    public function dispatch(QueryInterface $query)
    {
        $queryClass = get_class($query);
        if(!array_key_exists($queryClass, $this->handlers)) throw new \LogicException("Handler for query $queryClass not found");
        $handler = $this->handlers[$queryClass];
        return $handler->handle($query);
    }
}
