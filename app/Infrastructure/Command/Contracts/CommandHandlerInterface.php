<?php
namespace App\Infrastructure\Command\Contracts;


interface CommandHandlerInterface
{
    public function handle(CommandInterface $command);

    public function listen(): string;
}
