<?php

namespace App\Repositories;


use App\Models\InfrastructureExceptionModel;
use App\Repositories\Contracts\InfrastructureExceptionRepositoryInterface;
use Orkhanahmadov\EloquentRepository\EloquentRepository;

class InfrastructureExceptionRepository extends EloquentRepository implements InfrastructureExceptionRepositoryInterface
{
    protected $entity = InfrastructureExceptionModel::class;

    public function storeException(InfrastructureExceptionModel $exception): void
    {
        $this->create($exception->getAttributes());
    }
}
