<?php

namespace App\Repositories;

use App\Domain\Rent\Rent;
use App\Domain\Rent\RentPeriod;
use App\Exceptions\BusinessException;
use App\Models\RentModel;
use App\Repositories\Contracts\RentRepositoryInterface;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Orkhanahmadov\EloquentRepository\EloquentRepository;
use Ramsey\Uuid\UuidInterface;

class RentRepository extends EloquentRepository implements RentRepositoryInterface
{
    protected $entity = RentModel::class;

    public function fetchAll(): Collection
    {
        return $this->all()->transform(fn (RentModel $model) => (Rent::mapFromModel($model)));
    }

    public function createRent(Rent $rent): void
    {
        $model = RentModel::mapToModel($rent);
        $this->create($model->getAttributes());
    }

    public function countRentsByBikeAndPeriod(UuidInterface $bikeId, RentPeriod $period): int
    {
        return DB::table('rents')->select('id')->whereRaw("bike = ?", [$bikeId])
        ->where(function ($query) use ($period){
            $query->whereRaw("? BETWEEN start_date AND end_date", [$period->getStartDate()])
                ->orWhereRaw("? BETWEEN start_date AND end_date", [$period->getEndDate()])
                ->orWhereRaw("start_date BETWEEN ? AND ?", [$period->getStartDate(), $period->getEndDate()]);
        })->count('id');
    }

    public function deleteRent(UuidInterface $rentId): void
    {
        try {
            $this->findAndDelete($rentId);
        } catch (ModelNotFoundException $e) {
            throw new BusinessException('Sorry, this rent does not exist...');
        }
    }
}
