<?php

namespace App\Repositories\Contracts;


use App\Domain\Rent\Rent;
use App\Domain\Rent\RentPeriod;
use Illuminate\Support\Collection;
use Ramsey\Uuid\UuidInterface;

interface RentRepositoryInterface
{
    public function fetchAll(): Collection;

    public function createRent(Rent $rent): void;

    public function countRentsByBikeAndPeriod(UuidInterface $bikeId, RentPeriod $period): int;

    public function deleteRent(UuidInterface $rentId): void;
}
