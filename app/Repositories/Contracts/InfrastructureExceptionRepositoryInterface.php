<?php
namespace App\Repositories\Contracts;

use App\Models\InfrastructureExceptionModel;

interface InfrastructureExceptionRepositoryInterface
{
    public function storeException(InfrastructureExceptionModel $exception): void;
}
