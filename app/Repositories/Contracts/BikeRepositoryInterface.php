<?php
namespace App\Repositories\Contracts;

use App\Domain\Bike\Bike;
use Illuminate\Support\Collection;
use Ramsey\Uuid\UuidInterface;

interface BikeRepositoryInterface
{
    public function fetchAll(): Collection;
    public function findBikeById(UuidInterface $id): Bike;
    public function createBike(Bike $bike): void;
    public function updateBike(UuidInterface $bikeId, array $values): void;
}
