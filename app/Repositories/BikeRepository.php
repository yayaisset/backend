<?php

namespace App\Repositories;

use App\Domain\Bike\Bike;
use App\Exceptions\BusinessException;
use App\Models\BikeModel;
use App\Repositories\Contracts\BikeRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Orkhanahmadov\EloquentRepository\EloquentRepository;
use Ramsey\Uuid\UuidInterface;

class BikeRepository extends EloquentRepository implements BikeRepositoryInterface
{
    protected $entity = BikeModel::class;

    public function fetchAll(): Collection
    {
        return $this->all()->transform(fn (BikeModel $model) => (Bike::mapFromModel($model)));
    }

    public function createBike(Bike $bike): void
    {
        $model = BikeModel::mapToModel($bike);
        $this->create($model->getAttributes());
    }

    public function findBikeById(UuidInterface $id): Bike
    {
        /** @var BikeModel $model */
        $model = $this->find($id->toString());
        return Bike::mapFromModel($model);
    }

    public function updateBike(UuidInterface $bikeId, array $values): void
    {
        try {
            $this->findAndUpdate($bikeId, $values);
        } catch (ModelNotFoundException $e) {
            throw new BusinessException('Sorry, this bike does not exist');
        }
    }
}
