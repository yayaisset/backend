<?php
namespace App\Queries\Bike\Handlers;

use App\Infrastructure\Query\Contracts\QueryHandlerInterface;
use App\Infrastructure\Query\Contracts\QueryInterface;
use App\Queries\Bike\FetchAllBikesQuery;
use App\Repositories\Contracts\BikeRepositoryInterface;
use Illuminate\Support\Collection;

class FetchAllBikesQueryHandler implements QueryHandlerInterface
{
    private BikeRepositoryInterface $repository;

    public function __construct(BikeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(QueryInterface $query)
    {
        return Collection::make($this->repository->fetchAll());
    }

    public function listen(): string
    {
        return FetchAllBikesQuery::class;
    }
}
