<?php

namespace App\Queries\Bike\Handlers;


use App\Domain\Bike\Bike;
use App\Infrastructure\Query\Contracts\QueryHandlerInterface;
use App\Infrastructure\Query\Contracts\QueryInterface;
use App\Queries\Bike\FetchAllAvailableBikesForAPeriodQuery;
use App\Repositories\Contracts\BikeRepositoryInterface;
use App\Services\Bike\Contracts\BikeAvailabilityServiceInterface;
use Illuminate\Support\Collection;

class FetchAllAvailableBikesForAPeriodQueryHandler implements QueryHandlerInterface
{

    private BikeRepositoryInterface $repository;
    private BikeAvailabilityServiceInterface $service;

    public function __construct(BikeRepositoryInterface $repository, BikeAvailabilityServiceInterface $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @param QueryInterface|FetchAllAvailableBikesForAPeriodQuery $query
     */
    public function handle(QueryInterface $query)
    {
        $bikes = array_reduce($this->repository->fetchAll()->toArray(), function(array &$availableBikes, Bike $bike) use ($query){
            $bike = $this->service->getBikeWithAvailabilityForAPeriod($bike->id, $query->period);
            if ($bike->stock > 0) $availableBikes[] = $bike;
            return $availableBikes;
        },[]);

        return Collection::make($bikes);
    }

    public function listen(): string
    {
        return FetchAllAvailableBikesForAPeriodQuery::class;
    }
}
