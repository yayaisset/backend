<?php

namespace App\Queries\Bike;


use App\Domain\Rent\RentPeriod;
use App\Infrastructure\Query\Contracts\QueryInterface;

class FetchAllAvailableBikesForAPeriodQuery implements QueryInterface
{

    public RentPeriod $period;

    public function __construct(RentPeriod $period)
    {
        $this->period = $period;
    }
}
