<?php
namespace App\Queries\Rent\Handlers;

use App\Infrastructure\Query\Contracts\QueryHandlerInterface;
use App\Infrastructure\Query\Contracts\QueryInterface;
use App\Queries\Rent\FetchAllRentsQuery;
use App\Repositories\Contracts\RentRepositoryInterface;
use Illuminate\Support\Collection;

class FetchAllRentsQueryHandler implements QueryHandlerInterface
{

    private RentRepositoryInterface $repository;

    public function __construct(RentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function handle(QueryInterface $query)
    {
        return Collection::make($this->repository->fetchAll());
    }

    public function listen(): string
    {
        return FetchAllRentsQuery::class;
    }
}
