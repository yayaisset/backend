<?php

namespace App\Http\Controllers\Api\V1;

use App\Commands\Rent\CancelARentCommand;
use App\Commands\Rent\MakeARentCommand;
use App\Domain\Rent\Rent;
use App\Infrastructure\Bus\Contracts\CommandBusInterface;
use App\Infrastructure\Bus\Contracts\QueryBusInterface;
use App\Models\RentModel;
use App\Queries\Rent\FetchAllRentsQuery;
use App\Transformers\RentTransformer;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;


class RentController
{

    use Helpers;

    public function fetchAllRents(QueryBusInterface $queryBus) : Response
    {
        return $this->response->collection($queryBus->dispatch(new FetchAllRentsQuery()), new RentTransformer());
    }

    public function makeARent(Request $request, CommandBusInterface $commandBus) : Response
    {
        $values = json_decode($request->getContent(), true);
        $commandBus->dispatch(new MakeARentCommand(Rent::fromArray($values)));
        return $this->response->created();
    }

    public function cancelARent(string $rentId, CommandBusInterface $commandBus) : Response
    {
        $commandBus->dispatch(new CancelARentCommand(Uuid::fromString($rentId)));
        return $this->response->noContent()->statusCode(200);
    }
}
