<?php

namespace App\Http\Controllers\Api\V1;

use App\Commands\Bike\CreateBikeCommand;
use App\Commands\Bike\UpdateBikeStockCommand;
use App\Domain\Bike\Bike;
use App\Domain\Rent\RentPeriod;
use App\Infrastructure\Bus\Contracts\CommandBusInterface;
use App\Infrastructure\Bus\Contracts\QueryBusInterface;
use App\Queries\Bike\FetchAllAvailableBikesForAPeriodQuery;
use App\Queries\Bike\FetchAllBikesQuery;
use App\Transformers\BikeTransformer;
use Carbon\CarbonImmutable;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;


class BikeController
{
    use Helpers;

    public function fetchAllBikes(QueryBusInterface $queryBus) : Response
    {
        return $this->response->collection($queryBus->dispatch(new FetchAllBikesQuery()), new BikeTransformer());
    }

    public function fetchAllAvailableBikesForAPeriod(string $startDate, string $endDate, QueryBusInterface $queryBus) : Response
    {
        $rentPeriod = RentPeriod::fromDates(new CarbonImmutable($startDate), new CarbonImmutable($endDate));
        return $this->response->collection($queryBus->dispatch(new FetchAllAvailableBikesForAPeriodQuery($rentPeriod)), new BikeTransformer());
    }

    public function createBike(Request $request, CommandBusInterface $commandBus) : Response
    {
        $values = json_decode($request->getContent(), true);
        $commandBus->dispatch(new CreateBikeCommand(Bike::fromArray($values)));
        return $this->response->created();
    }

    public function updateBikeStock(string $bikeId, int $stock, CommandBusInterface $commandBus) : Response
    {
        // @TODO return custom exception on invalid stock type
        $commandBus->dispatch(new UpdateBikeStockCommand(Uuid::fromString($bikeId), $stock));
        return $this->response->noContent()->statusCode(200);
    }
}
